# Gret

Gret is a standalone ruby gem for monitoring and analysing public Git
repositories. It implements a bot that runs 24/7 and monitors git repositories
specified by the user. It offers analysis of user contributions to each project
based, mainly, on the user's commits to that project.

[![pipeline status](https://gitlab.com/flusp/msfloss/commit-bot/badges/master/pipeline.svg)](https://gitlab.com/flusp/msfloss/commit-bot)
[![coverage status](https://gitlab.com/flusp/msfloss/commit-bot/badges/master/coverage.svg)](https://gitlab.com/flusp/msfloss/commit-bot)

## Install Dependencies

Enter the cloned repository and run

    $ bundle install

If you have sudo problems, you may want to try:

    $ bundle install --path vendor/bundle

Which will install the gems at "vendor/bundle" (ignored by git).

## Usage

You can run the gem locally with:

    $ bundle exec bin/gret [-h|--help]
    $ bundle exec bin/gret [-v|--version]
    $ bundle exec bin/gret init [--path PATH]
    $ bundle exec bin/gret run [--path PATH] [--port PORT]
    $ bundle exec bin/gret config [--path PATH] --key KEY [--value VAL]

Commands description:

- _init_: to initialize a gret-tree at the given _path_ or, if no _path_ is
given, at the directory gret was invoked. The gret-tree is a set of directories
and files used by gret to track its state. You may resume a previous operation
on the same gret-tree or start a new one at an empty directory, for example.
- _run_: runs the server bot at the specified gret-tree (current directory or
_path_, if given). Note: gret will refuse to run more than one instance of the
bot in a same gret-tree. By default, the server is launched at port number 4567.
This can be changed through the _port_ argument.
- _config_: looks for the `gret.config` file in current directory or _path_,
if given, and allows inspection and modification of configurations. To inspect
a configuration, use `config --key <KEY>` and to modify it, use
`config --key <KEY> --value <VAL>`, where `<KEY>` is a configuration and `<VAL>` is the
value you want to set it to.
The key must not contain ':' even though configurations are represented as
symbols in the file.

Once `bin/gret run` is executed, server will be live at `localhost:4567`.

## Configuration File

`gret init` will create a `gret.config` file where default configurations for
that gret tree will reside. This file is written in YAML format. You may modify
these configurations as you wish. If any configuration is removed, gret will
use the default one.

These configurations can be modified as instructed in the section above.

## Docker

Alternatively, if you are satisfied with the defaults and just want to get
the server running, you can take the Docker approach.

All you need is `docker` and `docker-compose` installed. In the
project's root folder you can then run `docker-compose up`.
The first time this command is executed, it shall build the application's image.
Once it's done, the server should be up and running at `localhost:4567`.

## Exploring the API

### OAS and documentation

The servers' API is compliant with the
[OpenAPI Specification (OAS)](https://swagger.io/resources/open-api/),
which is under the Linux Foundations' OpenAPI Initiative. This is a widely
adopted standard for the definition of APIs, being supported by leading API
gateways like AWS, IBM, Apigee, and more.

This specification is comprised of a machine readable interface, through the
use of the YAML or JSON formats, making it easier for the consumption of our
API. Moreover, it allows for the generation of beautiful documentation.
Any service that follows the OAS specification can generate such interfaces,
such as [Swagger UI](https://swagger.io/tools/swagger-ui/),
[ReDoc](https://github.com/Rebilly/ReDoc), among others.

You can head over to our ReDoc page at
<https://gret-server-staging.herokuapp.com/> to check the documentation.
It should be able to generate a website with the most up to date usage of
the API, in which you can better understand its workings.

For a quick overview of our routes, see below.

### API summary

**Get list of tracked repositories**

`GET /api/v1/repositories`: returns a JSON with list of all tracked repos,
with fields `last_update`, `name`, `rid`, `url` and `status` for each entry.

Example command: `curl -X GET localhost:4567/api/v1/repositories`.

**Get info on a tracked repository**

`GET /api/v1/repositories/{rid}`: returns JSON with information of tracked repo
with `rid = {rid}` with fields `last_update`, `name`, `rid`, `url` and `status`.

Example command: `curl -X GET localhost:4567/api/v1/repositories/1`

**Start tracking new repository**

`POST /api/v1/repositories`: takes a JSON with `url` field and start tracking
repository with given URL. Also returns a JSON with fields `last_update`,
`name`, `rid`, `url` and `status`.

Example command:
`curl -X POST localhost:4567/api/v1/repositories
-d '{"url": "https://github.com/username/path/to/repository.git"}'`.

**Stop tracking a repository**

`DELETE /api/v1/repositories/{rid}`: deletes and stops tracking repository with `rid = {rid}`.

Example command: `curl -X DELETE localhost:4567/api/v1/repositories/1`.

#### Commits routes

**Get list of commits on a repository**

`GET /api/v1/commits`: returns JSON with commits information of a repository
with the given `rid`.

Example command: `curl -X GET "localhost:4567/api/v1/commits?rid=1`.

#### Contributors routes

**Get list of contributors to a repository**

`GET /api/v1/contributors`: returns JSON containing `email` and `name` of each
contributor of a repository with the given `rid`.

Example command: `curl -X GET "localhost:4567/api/v1/contributors?rid=1`.

## Installing the gem

You can install gret to your users' gems directory with:

    $ bundle exec rake install

Then, make sure gems' intallation path is in your $PATH env variable and check
installation with:

    $ gret --version

If it fails (and you use `bash`), adding the following line to your `.bashrc`
may solve the problem:

```bash
    export PATH="${PATH}:${HOME}/.gem/ruby/2.6.0/bin"
```

Replace `2.6.0` by your ruby version, if needed.
OBS: **you need at least ruby version 2.4.0!**

Note: tested on linux only. You may also want to check if that's the place
where gems are installed in your machine.

## Contributing

Commit messages should follow [this](https://chris.beams.io/posts/git-commit/)
format.

We use automatic testing and linting tools. Contributors are encouraged to
use the [pre-commit](`https://pre-commit.com`) python package, a framework for
managing and maintaining multi-language pre-commit hooks. After installing
`pre-commit` using your preferred installation method, run
```
pre-commit install -t pre-commit
pre-commit install -t commit-msg
pre-commit install -t pre-push
```
to set up git hooks to run `rubocop` before committing, `commitlint` to verify
commit messages, and `rspec` before pushing.

## Testing

To run our test suite, execute:

    $ bundle exec rake
