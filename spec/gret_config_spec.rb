require 'tmpdir'
require 'fileutils'
require 'yaml'

RSpec.describe Gret::GretConfig do
  before(:all) do
    @test_dir = Dir.mktmpdir()
    @test_path = File.join(@test_dir, 'test.yaml')
    File.write(@test_path, {key: 'value'}.to_yaml)
  end

  context "when initializing" do
    before(:all) do
      @old_path = Gret::GretConfig.path
      Gret::GretConfig.load @test_path
    end

    it "creates a yaml file given a path" do
      yaml_path = File.join(@test_dir, 'created.yaml')
      Gret::GretConfig.create yaml_path
      config = YAML.load_file(yaml_path)
      expect(config).to be_instance_of(Hash)
    end

    it "loads a yaml file given a path" do
      expect(YAML).to receive(:load_file)
      Gret::GretConfig.load @test_path
    end

    after(:all) do
      Gret::GretConfig.load @old_path
    end
  end

  context "when initialized" do
    before(:each) do
      @old_path = Gret::GretConfig.path
      Gret::GretConfig.load @test_path
    end

    it "fallsback to default configurations" do
      expect{ Gret::GretConfig.get :async_clone }.to_not raise_error(RuntimeError)
    end

    it "returns the current value of a configuration" do
      expect(Gret::GretConfig.get :key).to eq('value')
    end

    it "raises an error if asked for a nonexistent configuration" do
      expect{ Gret::GretConfig.get :nonexistent }.to raise_error(RuntimeError)
    end

    it "writes new configurations to disk" do
      Gret::GretConfig.set :new_key, 'new_value'
      yaml_hash = YAML.load_file(@test_path)
      expect(yaml_hash[:new_key]).to eq('new_value')
    end

    after(:each) do
      Gret::GretConfig.load @old_path
    end
  end

  after(:all) do
    FileUtils.rm_r(@test_dir)
  end
end
