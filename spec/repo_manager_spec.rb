require 'tmpdir'
require 'fileutils'
require 'rugged'
require 'sequel'

RSpec.configure do |c|
  c.around(:each) do |example|
    Sequel::Model.db.transaction(rollback: :always, auto_savepoint: true) {
      example.run
    }
  end
end

RSpec.describe Gret::RepoManager do
  before(:all) do
    @git_directory = Dir.mktmpdir()
    Rugged::Repository.init_at(@git_directory)
  end

  context "when adding repositories" do
    it "clones repositories given a url and returns a Hash" do
      expect(
        Gret::RepoManager.clone @git_directory
      ).to be_instance_of(Hash)
      expect(
        Model::RepoTracker.get_from_url(@git_directory).first[:status]
      ).to eq("tracking")
    end

    it "sets status to 'clone failed' if the given url does not exist" do
      Gret::RepoManager.clone "foobar"
      expect(
        Model::RepoTracker.get_from_url("foobar").first[:status]
      ).to eq("clone failed")
    end

    it "raises an exception if clonning a repository twice" do
        Gret::RepoManager.clone @git_directory
        expect {
          Gret::RepoManager.clone @git_directory
        }.to raise_error(TypeError)
    end

    after(:each) do
      FileUtils.rm_r(Dir[File.join(Gret::GretTree.instance.repos_path, '*')])
    end
  end

  context "when managing existing repositories" do
    before(:each) do
      Model::RepoTracker.put(
        url: @git_directory, status: "tracking", last_update: DateTime.now
      )
      @rid = Model::RepoTracker.get_from_url(@git_directory).first[:rid]
      Rugged::Repository.clone_at(
        @git_directory, Gret::RepoManager.path(@rid), {bare: true}
      )
    end

    it "raises an error if trying to clone with an existing url" do
      expect {
        Gret::RepoManager.clone @git_directory
      }.to raise_error(TypeError)
    end

    it "tells if a repository with a given rid is already in the database" do
      expect(Gret::RepoManager.exists? @rid).to be true
      expect(Gret::RepoManager.exists? @rid+1).to be false
    end

    it "tells if a repository with a given rid is available to query" do
      expect(Gret::RepoManager.available_to_query? @rid).to be true
      expect(Gret::RepoManager.available_to_query? @rid+1).to be false
    end

    it "returns a stored repository as a rugged object" do
      expect(
        Gret::RepoManager.rugged_repository @rid
      ).to be_instance_of(Rugged::Repository)
    end

    it "raises an error if the queried repository is not available" do
      expect {
        Gret::RepoManager.rugged_repository @rid+1
      }.to raise_error(ArgumentError)
    end

    it "successfuly removes an existing repository with a given rid" do
      Gret::RepoManager.remove @rid
      expect(
        (not Dir.exist?(Gret::RepoManager.path @rid) and
        Model::RepoTracker.get(@rid).first.nil?)
      ).to be true
    end

    it "does not cause a crash if asked to remove a non existing repo" do
      expect {
        Gret::RepoManager.remove @rid+1
      }.to_not raise_error
    end

    after(:each) do
      # Remove the repositories/* contents. Note that the removal of the
      # rescpective entries on the DB is already performed by the transaction
      # rollback mechanism (See the top of this file).
      FileUtils.rm_r(Dir[File.join(Gret::GretTree.instance.repos_path, '*')])
    end
  end

  after(:all) do
    FileUtils.rm_r(@git_directory)
  end
end
