require 'spec_helper'
require 'sequel'
require 'fileutils'
require 'pathname'

RSpec.configure do |c|
  c.around(:each) do |example|
    Sequel::Model.db.transaction(
      rollback: :always, auto_savepoint: true){ example.run }
  end
end

RSpec.describe "Model::RepoTracker" do
  it "should be able to put new entries in the repo_trackers table" do
    Model::RepoTracker.put(url: "https://a.com.br/top.git",
                           status: "cloning",
                           last_update: DateTime.now)
    expect(Model::RepoTracker.all).not_to be_empty
  end

  context "when managing existing repo_trackers" do
    before(:each) do
      Model::RepoTracker.put(url: "https://a.com.br/top.git",
                             status: "cloning",
                             last_update: DateTime.now)
      @rid = Model::RepoTracker.dataset[url: "https://a.com.br/top.git"][:rid]
    end

    it "should be able to retrieve repo_trackers row using rid" do
      # using .first because the map method returns an array
      expect(
        Model::RepoTracker.get(@rid).map(:url).first
      ).to eq("https://a.com.br/top.git")
    end

    it "should be able to retrieve repo_trackers row using URL" do
      expect(
        Model::RepoTracker.get_from_url("https://a.com.br/top.git").map(:rid).first
      ).to eq(@rid)
    end

    it "should not be able to create two repo_tracker with same URL" do
      duplicate_repo = {url: "https://a.com.br/top.git",
                        status: "cloning",
                        last_update: DateTime.now}
      # need to use a block here
      expect{
        Model::RepoTracker.put(duplicate_repo)
      }.to raise_error(Sequel::ValidationFailed)
    end

    it "should be able to update an existing repo_tracker using rid" do
      Model::RepoTracker.update(@rid, {url: "https://b.com.br/top.git",
                                       status: "clone_failed"})
      expect(
        Model::RepoTracker.get(@rid).map(:url).first
      ).to eq("https://b.com.br/top.git")
    end

    it "should not be able to update a repo_tracker to an existing URL" do
      Model::RepoTracker.put(
        url: "https://c.com.br/top.git",
        status: "cloning",
        last_update: DateTime.now)
      expect{
        Model::RepoTracker.update(
          @rid, {url: "https://c.com.br/top.git", status: "clone_failed"})
      }.to raise_error(Sequel::UniqueConstraintViolation)
    end
  end
end
