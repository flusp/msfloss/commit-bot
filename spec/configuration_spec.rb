# frozen_string_literal: true

require 'rspec/expectations'
require 'fileutils'

RSpec.configure do |config|
  config.before(:suite) do
    Gret::GretTree.create(Dir.mktmpdir)
    Gret::GretConfig.set :async_clone, false
  end

  config.after(:suite) do
    FileUtils.rm_rf(Gret::GretTree.instance.path)
  end
end
