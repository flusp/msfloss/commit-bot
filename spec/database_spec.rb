require 'spec_helper'
require 'sequel'
require 'fileutils'
require 'pathname'

RSpec.describe Gret::DatabaseConfig do
  context "when creating the database" do
    before(:all) do
      @db_dir = "spec_db"
      FileUtils.mkdir @db_dir
      Gret::DatabaseConfig.connect_database(@db_dir)
      @db = Sequel::Model.db
    end    

    it "should create a sqlite gret.db file" do
      expect(Pathname.new(@db_dir+"/gret.db")).to be_file
    end

    it "should create a DB connection instance in sequel module" do
      expect(Sequel::Model.db).to be_instance_of Sequel::SQLite::Database
    end

    it "should be able to correctly create new schemas" do
      @db.create_table :test do
        primary_key :id
        String :name, unique: true, null: false
        TrueClass :active, default: true
        DateTime :created_at, default: Sequel::CURRENT_TIMESTAMP, :index=>true              
      end
      schema_attrs = @db.schema(:test).flatten.select.with_index {|_, i| i.even?}
      expect(schema_attrs).to match_array([:id, :name, :active, :created_at])
    end

    after(:all) do
      @db.disconnect
      FileUtils.rm_rf(@db_dir)
    end
  end
  context "when using setup_database" do
    before(:all) do
      @db_dir = "spec_db"
      FileUtils.mkdir @db_dir
      Gret::DatabaseConfig.setup_database(@db_dir)
    end    

    before(:each) do
      @db = Gret::DatabaseConfig.connect_database(@db_dir)
    end

    it "should create the repo_trackers table" do
      expect(@db.schema(:repo_trackers)).not_to be_empty
    end


    after(:each) do
      @db.disconnect
    end

    after(:all) do
      FileUtils.rm_rf(@db_dir)
    end
  end
end
