require 'tmpdir'
require 'sequel'
require 'rack/test'
require 'fileutils'

RSpec.describe Gret::Routing do
  include Rack::Test::Methods
  def app
    Gret::Routing
  end

  ver = 'v1'
  base_url = "/api/#{ver}/"

  #-----------------------
  # Repositories Endpoint
  #-----------------------
  context "when sending requests to /repositories endpoint" do
    endpoint = 'repositories'
    url = base_url+endpoint
    rid = '1'

    it "returns all tracked repositories" do
      allow(Gret::ApiController).to receive(:all_repositories).and_return(Gret::ApiController::generate_response(200,'[]'))

      get url
      expect(last_response.ok?).to be true
      expect(last_response.body).to eq '[]'
    end

    it "returns a specific tracked repository" do
      allow(Gret::ApiController).to receive(:get_repository).and_return(Gret::ApiController::generate_response(200,'Ok'))

      get url+'/'+rid
      expect(last_response.ok?).to be true
      expect(last_response.body).to eq 'Ok'
    end

    it "triggers repository tracking" do
      @json_post = {:url => 'http://valid.url.com'}.to_json
      allow(Gret::ApiController).to receive(:add_repository).and_return(Gret::ApiController::generate_response(200,"Ok"))

      post(url, @json_post, { 'CONTENT_TYPE' => 'application/json' })
      expect(last_response.ok?).to be true
      expect(last_response.body).to eq "Ok"
    end

    it "stops tracking and deletes repository" do
      allow(Gret::ApiController).to receive(:delete_repository).and_return(Gret::ApiController::generate_response(200,"Ok"))

      delete url+'/'+rid
      expect(last_response.ok?).to be true
      expect(last_response.body).to eq "Ok"
    end

    it "does not return exceptions to the user when configured" do
      old_config_path = Gret::GretConfig.path
      new_path = Dir.mktmpdir
      new_config_path = File.join(new_path, 'test.yaml')
      Gret::GretConfig.create new_config_path
      Gret::GretConfig.load new_config_path
      Gret::GretConfig.set :show_exceptions, false

      allow(Gret::ApiController).to receive(:all_repositories).and_raise('Error')

      expect { get url }.not_to raise_error
      expect(last_response.ok?).to be false
      expect(last_response.body).to eq '<h1>Internal Server Error</h1>'

      Gret::GretConfig.load old_config_path
      FileUtils.rm_r(new_path)
    end
  end

  #------------------
  # Commits Endpoint
  #------------------
  context "when sending requests to /commits endpoint" do
    endpoint = 'commits'
    url = base_url+endpoint
=begin
    it "returns all commits from given repository" do
      allow(Gret::ApiController).to receive(:get_commits).and_return(
        Gret::ApiController::generate_response(200,"Ok")
      )

      expect(Gret::ApiController).to receive(:get_commits).with(
        '1', hash_including(
          initial_date: nil, final_date: nil, limit: nil, sort: nil
        )
      )
      get url, params={:rid => '1'}
      expect(last_response.ok?).to be true
      expect(last_response.body).to eq "Ok"
    end
=end
  end

  #-----------------------
  # Contributors Endpoint
  #-----------------------
  context "when sending requests to /contributors endpoint" do
    endpoint = 'contributors'
    url = base_url+endpoint

    it "returns all contributors from given repository" do
      allow(Gret::ApiController).to receive(:get_contributors).and_return(Gret::ApiController::generate_response(200,"Ok"))

      get url, params={:rid => '1'}
      expect(last_response.ok?).to be true
      expect(last_response.body).to eq "Ok"
    end

  end
end
