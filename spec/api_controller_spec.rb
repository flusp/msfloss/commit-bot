# frozen_string_literal: true

require 'tmpdir'
require 'fileutils'
require 'rugged'
require 'sequel'
require 'json'
require 'net/http'

RSpec.describe Gret::ApiController do
  before(:all) do
    @git_directory = Dir.mktmpdir
    @repo = Rugged::Repository.init_at(@git_directory, :bare)

    authors = [{ email: 'a@gmail.com', time: '2018-01-01', name: 'A' },
               { email: 'b@gmail.com', time: '2018-08-01', name: 'B' },
               { email: 'c@gmail.com', time: '2019-05-01', name: 'C' }]

    parse_time = lambda do |date_str|
      Time.parse(Date.strptime(date_str, '%Y-%m-%d').to_s)
    end
    @commit_oids = authors.map do |author|
      curr_time = author[:time] = parse_time.call(author[:time])
      index = @repo.index
      oid = @repo.write("Some content for this blob - #{curr_time}.", :blob)
      index.add(path: 'file.txt', oid: oid, mode: 0o100644)
      curr_tree = index.write_tree(@repo)
      Rugged::Commit.create(
        @repo,
        author: author,
        message: "Some commit subject at #{curr_time}\n\nSome commit body.",
        committer: author,
        parents: @repo.empty? ? [] : [@repo.head.target],
        tree: curr_tree,
        update_ref: 'HEAD'
      )
    end
  end

  before(:each) do
    allow(Gret::RepoManager).to receive(:exists?).and_return(true)
    allow(Gret::RepoManager).to receive(:rugged_repository).and_return(@repo)
  end

  #----------------------------
  # Repository Controller Tests
  #-----------------------------
  context "when managing repositories" do
    it "tracks repository with valid url" do
      url = "https://valid.url.com/this/git.git"
      repo_info = {:url => url, :status => "cloning", :last_update => DateTime.now}
      allow(Gret::RepoManager).to receive(:clone).and_return(
        repo_info)

      response = Gret::ApiController.add_repository url
      expect(response['status']).to be 200
      expect(response['message']).to eq repo_info.to_json
    end

    it "doesn't track already tracked repositories" do
      url = "https://valid.url.com/this/git.git"
      msg = "#{url} is already being tracked or cloned"
      allow(Gret::RepoManager).to receive(:clone).and_return(msg)

      response = Gret::ApiController.add_repository url
      expect(response['status']).to be 200
      expect(response['message']).to include msg
    end

    it "doesn't track invalid url" do
      url = "this/invalidurl"
      error_msg = {:error => "invalid url"}

      response = Gret::ApiController.add_repository url
      expect(response['status']).to be 405
      expect(response['message']).to eq error_msg.to_json
    end

    it "deletes a tracked repository" do
      allow(Gret::RepoManager).to receive(:remove).and_return(true)
      response = Gret::ApiController.delete_repository "1"
      success_msg = {:message => "repository 1 deletion successful"}

      expect(response['status']).to be 200
      expect(response['message']).to eq success_msg.to_json
    end

    it "warns of incorrect rid parameter" do
      response = Gret::ApiController.get_repository "abc"
      error_msg = {:error => "rid must be an integer"}

      expect(response['status']).to be 400
      expect(response['message']).to eq error_msg.to_json
    end

  end

  #-------------------------
  # Commits Controller Tests
  #--------------------------
  context "when managing commits from clonned repositories" do
    it "gets all commits" do
      commits = JSON.parse(Gret::ApiController.get_commits('1')['message'])
      expect(commits).to be_instance_of(Array)
      expect(
        commits.map { |commit| commit['id'] }.to_set
      ).to eq(@commit_oids.to_set)
    end

    it "warns of incorrect rid parameter" do
      commits = Gret::ApiController.get_commits "abc"
      error_msg = {:error => "rid must be an integer"}

      expect(commits['status']).to equal 400
      expect(commits['message']).to eq error_msg.to_json
    end
=begin
    it "correctly fills in commit data" do
      commit = JSON.parse(Gret::ApiController.get_commits('1')['message']).last
      expect(commit['author']['email']).to include('@gmail.com')
      expect(commit['message']).to include('Some commit body')
      expect(commit['subject']).to include('Some commit subject')
      expect(commit['subject']).not_to include('Some commit body')
      expect(commit['diff']).to include('Some content for this blob')
      expect(commit['date']).not_to be nil
      expect(commit['id']).not_to be nil
    end
=end
    it "does not return repeated commits" do
      commits = JSON.parse(Gret::ApiController.get_commits('1')['message'])
      oid_set = Set.new
      repeated = false
      commits.each do |commit|
        repeated = true if oid_set.include? commit['id']
        oid_set << commit['id']
      end
      expect(repeated).to be false
    end
=begin
    it "correctly limit commits returned" do
      commits = JSON.parse(
        Gret::ApiController.get_commits(
          '1', limit: 2
        )['message']
      )
      expect(commits.length).to eq 2
    end

    it "correctly filters commits by date" do
      commits = JSON.parse(
        Gret::ApiController.get_commits(
          '1', initial_date: '2018-01-01', final_date: '2018-12-12'
        )['message']
      )
      expect(commits.length).to eq 2
      expect(commits.map { |el| el['author']['name'] }.to_set).to eq %w[B A].to_set
    end

    it "returns no commits when date period is offset" do
      commits = JSON.parse(
        Gret::ApiController.get_commits(
          '1', initial_date: '2090-01-01', final_date: '2091-01-01'
        )['message']
      )
      expect(commits).to be_instance_of(Array)
      expect(commits.length).to eq 0
    end

    it "returns error with wrong date parameter format" do
      result = JSON.parse(
        Gret::ApiController.get_commits(
          '1', initial_date: 'a', final_date: '2018-12-12'
        )['message']
      )
      expect(result).to be_instance_of(Hash)
      expect(result['error']).not_to be nil
      expect(result['error']).to include('date format provided is wrong')
    end

    it "returns error when one date parameter is missing" do
      result = JSON.parse(
        Gret::ApiController.get_commits(
          '1', final_date: '2018-12-12'
        )['message']
      )
      expect(result).to be_instance_of(Hash)
      expect(result['error']).not_to be nil
      expect(result['error']).to include('one date parameter is missing')
    end
=end

  end
  #-----------------------------
  # Contributors Controler Tests
  #-----------------------------
  context "when managing contributors from clonned repositories" do
    it "gets all contributors" do
      contributors = JSON.parse(
        Gret::ApiController.get_contributors('1')['message']
      )
      expect(contributors).to be_instance_of(Array)
      expect(contributors[0]).not_to be nil
      expect(
        contributors.map { |el| el['name'] }.to_set
      ).to eq(%w[B A C].to_set)
      expect(contributors[0]['email']).not_to be nil
      expect(contributors[0]['name']).not_to be nil
      expect(contributors[0]['commits']).not_to be nil
    end

    it "correctly filters contributors by date" do
      contributors = JSON.parse(
        Gret::ApiController.get_contributors(
          '1', initial_date: '2018-01-01', final_date: '2018-12-12'
        )['message']
      )
      expect(contributors.length).to eq 2
      expect(contributors.map { |el| el['name'] }.to_set).to eq %w[B A].to_set
    end

    it "returns no contributors when date period is offset" do
      contributors = JSON.parse(
        Gret::ApiController.get_contributors(
          '1', initial_date: '2090-01-01', final_date: '2091-01-01'
        )['message']
      )
      expect(contributors).to be_instance_of(Array)
      expect(contributors.length).to eq 0
    end

    it "returns error with wrong date parameter format" do
      result = JSON.parse(
        Gret::ApiController.get_contributors(
          '1', initial_date: 'a', final_date: '2018-12-12'
        )['message']
      )
      expect(result).to be_instance_of(Hash)
      expect(result['error']).not_to be nil
      expect(result['error']).to include('date format provided is wrong')
    end

    it "returns error when one date parameter is missing" do
      result = JSON.parse(
        Gret::ApiController.get_contributors(
          '1', final_date: '2018-12-12'
        )['message']
      )
      expect(result).to be_instance_of(Hash)
      expect(result['error']).not_to be nil
      expect(result['error']).to include('one date parameter is missing')
    end

  end

  after(:all) do
    FileUtils.rm_r(@git_directory)
  end
end
