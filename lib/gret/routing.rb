require 'sinatra'
require 'json'
require 'yaml'
require_relative '../swagger/swaggering'

module Gret
  class Routing < Swaggering
    set :show_exceptions, -> { Gret::GretConfig.get(:show_exceptions) }

    self.add_route('GET', '/',{
      }) do
      cross_origin

      File.read(File.join('public', 'api_doc.html'))
    end

    self.add_route('GET', '/resources',{
      }) do
      cross_origin

      JSON.pretty_generate(YAML.load(File.read('./swagger.yaml')))
    end

    #---------------------
    # Repositories Routes
    #---------------------
    self.add_route('DELETE', '/api/v1/repositories/{rid}', {
      "resourcePath" => "/Repositories",
      "summary" => "Delete repository by rid",
      "nickname" => "delete_repository",
      "responseClass" => "void",
      "endpoint" => "/repositories/{rid}",
      "notes" => "",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "Repository id to delete",
          "dataType" => "int",
          "paramType" => "path",
        },
        {
          "name" => "api_key",
          "description" => "",
          "dataType" => "string",
          "paramType" => "header",
        },
        ]}) do
      cross_origin

        resp = Gret::ApiController.delete_repository(params['rid'])
        status resp['status']
        body resp['message']
        resp['message']
    end


    self.add_route('GET', '/api/v1/repositories', {
      "resourcePath" => "/Repositories",
      "summary" => "Returns all repositories being tracked and their information",
      "nickname" => "get_repositories",
      "responseClass" => "array[RepositoryResponse]",
      "endpoint" => "/repositories",
      "notes" => "",
      "parameters" => [
        ]}) do
      cross_origin

        resp = Gret::ApiController.all_repositories
        status resp['status']
        body resp['message']
        resp['message']
    end


    self.add_route('GET', '/api/v1/repositories/{rid}', {
      "resourcePath" => "/Repositories",
      "summary" => "Find repository information by rid",
      "nickname" => "get_repository_by_rid",
      "responseClass" => "RepositoryResponse",
      "endpoint" => "/repositories/{rid}",
      "notes" => "Returns a single repository",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "Rid of the repository",
          "dataType" => "int",
          "paramType" => "path",
        },
        ]}) do
      cross_origin

        resp = Gret::ApiController.get_repository(params['rid'])
        status resp['status']
        body resp['message']
        resp['message']
      end


    self.add_route('POST', '/api/v1/repositories', {
      "resourcePath" => "/Repositories",
      "summary" => "Add a new repository to be tracked",
      "nickname" => "track_repo",
      "responseClass" => "RepositoryResponse",
      "endpoint" => "/repositories",
      "notes" => "The Git Repositories Tracker requires a name and URL in order" +
                 "to start its tracking service. This is done via cloning the" +
                 "repository. Take note that big repositories may take a while" +
                 "to be cloned, even though the cloning is done with the" +
                 "`--bare` option.",
      "parameters" => [
        {
          "name" => "body",
          "description" => "Repository location and branch to be tracked",
          "dataType" => "RepositoryPost",
          "paramType" => "body",
        }
        ]}) do
      cross_origin

        parsed_req = JSON.parse(request.body.read)
        resp = Gret::ApiController.add_repository parsed_req['url']
        status resp['status']
        body resp['message']
        resp['message']
    end


    #---------------------
    # Commits Routes
    #---------------------
    self.add_route('GET', '/api/v1/commits', {
      "resourcePath" => "/Commits",
      "summary" => "Returns list of commits of given repository",
      "nickname" => "get_commits_from_repository",
      "responseClass" => "array[CommitResponse]",
      "endpoint" => "/commits",
      "notes" => "",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "Rid of the repository",
          "dataType" => "int",
          "paramType" => "query",

          "allowableValues" => "",

        },
        ]}) do
      cross_origin
        resp = Gret::ApiController.get_commits(
          params['rid'],
        )
        status resp['status']
        body resp['message']
        resp['message']
    end

    self.add_route('GET', '/api/v1/mycommits', {
      "resourcePath" => "/Commits",
      "summary" => "Returns a user list of commits",
      "nickname" => "get_diffs_from_commits",
      "responseClass" => "array[CommitResponse]",
      "endpoint" => "/mycommits",
      "notes" => "",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "Rid of the repository",
          "dataType" => "int",
          "paramType" => "query",

          "allowableValues" => "",

        },
        ]}) do
      cross_origin
        resp = Gret::ApiController.get_my_commits(
          params['rid'],
          params['uemail'],
        )
        status resp['status']
        body resp['message']
        resp['message']
    end

    self.add_route('GET', '/api/v1/commits/diffs', {
      "resourcePath" => "/Commits",
      "summary" => "Returns list diffs",
      "nickname" => "get_diffs_from_commits",
      "responseClass" => "array[CommitResponse]",
      "endpoint" => "/commits/diffs",
      "notes" => "",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "Rid of the repository",
          "dataType" => "int",
          "paramType" => "query",

          "allowableValues" => "",

        },
        ]}) do
      cross_origin
        resp = Gret::ApiController.get_diffs(
          params['rid'],
        )
        status resp['status']
        body resp['message']
        resp['message']
    end

    self.add_route('GET', '/api/v1/commits/simplediffs', {
      "resourcePath" => "/Commits",
      "summary" => "Returns list of simple diffs",
      "nickname" => "get_diffs_from_commits",
      "responseClass" => "array[CommitResponse]",
      "endpoint" => "/commits/simplediffs",
      "notes" => "",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "Rid of the repository",
          "dataType" => "int",
          "paramType" => "query",

          "allowableValues" => "",

        },
      ]}) do
      cross_origin
      resp = Gret::ApiController.get_simplediffs(
        params['rid'],
      )
      status resp['status']
      body resp['message']
      resp['message']
    end

    

    


    #---------------------
    # Contributors Routes
    #---------------------
    self.add_route('GET', '/api/v1/contributors', {
      "resourcePath" => "/Contributors",
      "summary" => "Returns list of contributors of given repositories",
      "nickname" => "get_contributors_from_repository",
      "responseClass" => "array[ContributorResponse]",
      "endpoint" => "/contributors",
      "notes" => "",
      "parameters" => [
        {
          "name" => "rid",
          "description" => "The rid of the desired repository",
          "dataType" => "int",
          "paramType" => "query",

          "allowableValues" => "",

        },
        {
          "name" => "initial_date",
          "description" => "Start date-time filter (yyyy-mm-dd)",
          "dataType" => "DateTime",
          "paramType" => "query",

          "allowableValues" => "",

        },
        {
          "name" => "final_date",
          "description" => "Final date-time filter (yyyy-mm-dd)",
          "dataType" => "DateTime",
          "paramType" => "query",

          "allowableValues" => "",

        },
        ]}) do
      cross_origin

        resp = Gret::ApiController.get_contributors(
          params['rid'],
          initial_date: params['initial_date'],
          final_date: params['final_date']
        )
        status resp['status']
        body resp['message']
        resp['message']
    end

  end
end
