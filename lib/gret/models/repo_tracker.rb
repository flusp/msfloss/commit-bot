require 'sequel'

module Model
  # this is a class for repo_trackers table
  class RepoTracker < Sequel::Model
    def self.dataset
      Sequel::Model.db[:repo_trackers]
    end

    plugin :validation_helpers
    def validate
      super # basic validation
      validates_unique :url, :message => 'is not a unique repo URL'
    end

    # Class methods to interact with the table as a whole
    def self.put repo
      new_repo = self.new(repo)
      new_repo.save()
    end

    def self.get rid
      self.dataset.where(:rid => rid)
    end

    def self.get_from_url url
      self.dataset.where(:url => url)
    end

    def self.update rid, updates
      self.dataset.where(:rid => rid).update(updates)
    end

    def self.all
      self.dataset.order(:last_update).all
    end
  end
end
