require 'yaml'
require_relative 'gret_tree'


module Gret
  # Note: GretConfig.load must be called to initialize this class. It's
  # an error to use the class before doing it.
  class GretConfig

    @@default_configs = {
      :async_clone => true,
      :show_exceptions => true
    }

    @@configs = nil
    @@path = nil

    def self.create path
      self.write_configs! @@default_configs, path
    end

    def self.load path
      @@path = path
      @@configs = (YAML.load_file(@@path) or {})
    end

    def self.path
      @@path
    end

    def self.get key
      if @@configs.key? key
        return @@configs[key]
      elsif @@default_configs.key? key
        return @@default_configs[key]
      else
        raise "Invalid config key #{key}"
      end
    end

    def self.set key, value
      @@configs[key] = value
      self.write!
    end

    def self.write!
      self.write_configs! @@configs, @@path
    end

    private_class_method
    def self.write_configs! configs, path
      to_write = configs.to_yaml
      if File.write(path, to_write) != to_write.length
        raise "Failed to write configs to #{path}"
      end
    end

  end
end
