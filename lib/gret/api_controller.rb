require 'sequel'
require 'uri'
require 'rugged'
require 'date'

module Gret
  class ApiController
    def self.generate_response(status = 200, message = "OK")
      {"status" => status, "message" => message}
    end

    def self.validate_rid rid
      if rid =~ /^[-+]?[1-9]([0-9]*)?$/
        if Gret::RepoManager.exists? rid
          resp = generate_response(200, "OK")
        else
          resp = generate_response(404, [].to_json)
        end
      else
        resp = generate_response(400, {"error" => "rid must be an integer"}.to_json)
      end
      resp
    end

    def self.valid_uri? url
      url =~ /\A#{URI::regexp}\z/ ? true : false
    end

    def self.valid_datetime? datetime_str
      begin
        Date.strptime(datetime_str, "%Y-%m-%d")
      rescue ArgumentError
        return false
      end
      return true
    end

    def self.validate_date_params initial_date, final_date
      dates = [initial_date != nil, final_date != nil]
      if dates.any? && !dates.all?
        return generate_response(400, {"error" => "one date parameter is missing"}.to_json)
      elsif dates.all?
        if [initial_date, final_date].map{|d| valid_datetime? d}.all?
          return generate_response(200, "OK")
        else
          return generate_response(400, {"error" => "date format provided is wrong"}.to_json)
        end
      end
    end


    #-------------------------
    # Repositories Controller
    #-------------------------
    def self.repository_operation rid
      resp = validate_rid rid
      if resp["status"] != 200
        return resp
      end
      begin
        data = yield
        if data == true
          resp = generate_response(200, {"message" => "repository #{rid} deletion successful"}.to_json)
        elsif data.all.any?
          resp = generate_response(200, data.all.first.to_json)
        end
      rescue ArgumentError => e
        puts e
        resp = generate_response(status = 400, message = {"error" => e})
      end
      resp
    end

    def self.delete_repository rid
      repository_operation(rid) { Gret::RepoManager.remove rid }
    end

    def self.get_repository rid
      repository_operation(rid) { Model::RepoTracker.get rid }
    end

    def self.all_repositories
      generate_response(200, Model::RepoTracker.all.to_json)
    end

    def self.add_repository url
      begin
        if valid_uri? url
          data = Gret::RepoManager.clone(url)
          resp = generate_response(200, data.to_json)
        else
          resp = generate_response(405, {"error" => "invalid url"}.to_json)
        end
      rescue TypeError => e
        resp = generate_response(400, {"error" => e}.to_json)
        puts e
      end
      resp
    end

    #-------------------
    # Commits Controller
    #-------------------
    def self.get_commits(rid)
      resp = validate_rid rid
      if resp["status"] != 200
        return resp
      end

      begin
        commits = []
        repo = Gret::RepoManager.rugged_repository rid
        walker = Rugged::Walker.new(repo)
        walker.push(repo.head.target)
        count = 0
        walker.each do |commit|
          puts count
          count = count + 1
          commits.push(
            { id: commit.oid,
              date: Time.at(commit.epoch_time).to_datetime,
              author_email: commit.author[:email].encode('utf-8', invalid: :replace, undef: :replace),
              commiter_name: commit.committer[:name].encode('utf-8', invalid: :replace, undef: :replace),
              subject: commit.summary.encode('utf-8', invalid: :replace, undef: :replace),
            })
        end
        resp = generate_response(200, commits.to_json)
      rescue TypeError => e
        resp = generate_response(400, {"error" => e}.to_json)
        puts e
      end
      resp
    end

    def self.get_my_commits(rid, uemail)
      resp = validate_rid rid
      if resp["status"] != 200
        return resp
      end

      begin
        commits = []
        repo = Gret::RepoManager.rugged_repository rid
        walker = Rugged::Walker.new(repo)
        walker.push(repo.head.target)
        count = 0
        walker.each do |commit|
          count = count + 1
          if commit.parents.empty?
            commit_diff = commit.diff.patch
          else
            commit_diff = commit.parents.first.diff(commit)
          end
          auth_email = commit.author[:email].encode('utf-8', invalid: :replace, undef: :replace)
          puts uemail 
          puts auth_email
          if auth_email == uemail
            commits.push(
            {  
              id: commit.oid,
              date: Time.at(commit.epoch_time).to_datetime,
              author_email: auth_email,
              commiter_name: commit.committer[:name].encode('utf-8', invalid: :replace, undef: :replace),
              subject: commit.summary.encode('utf-8', invalid: :replace, undef: :replace),
            })
          end
        end
    
        resp = generate_response(200, commits.to_json)
      rescue TypeError => e
        resp = generate_response(400, {"error" => e}.to_json)
        puts e
      end
      resp
    end

    def self.get_diffs(rid)
      resp = validate_rid rid
      if resp["status"] != 200
        return resp
      end

      begin
        commits = []
        repo = Gret::RepoManager.rugged_repository rid
        walker = Rugged::Walker.new(repo)
        walker.push(repo.head.target)
        count = 0
        walker.each do |commit|
          count = count + 1
          if commit.parents.empty?
            commit_diff = commit.diff.patch
          else
            commit_diff = commit.parents.first.diff(commit)
            files, additions, deletions = commit_diff.stat
          end
          commits.push(
          { 
            date: Time.at(commit.epoch_time).to_datetime,
            author_name: commit.author[:email].encode('utf-8', invalid: :replace, undef: :replace),
            added_lines: additions,
            deleted_lines: deletions
          })
        end
    
        resp = generate_response(200, commits.to_json)
      rescue TypeError => e
        resp = generate_response(400, {"error" => e}.to_json)
        puts e
      end
      resp
    end

    def self.get_simplediffs(rid)
      resp = validate_rid rid
      if resp["status"] != 200
        return resp
      end

      begin
        commits = []
        repo = Gret::RepoManager.rugged_repository rid
        walker = Rugged::Walker.new(repo)
        walker.push(repo.head.target)
        count = 0
        walker.each do |commit|
          count = count + 1
          
          commits.push(
          { 
            date: Time.at(commit.epoch_time).to_datetime,
            author_email: commit.author[:email].encode('utf-8', invalid: :replace, undef: :replace),
            commiter_email: commit.committer[:email].encode('utf-8', invalid: :replace, undef: :replace),
          })
        end
    
        resp = generate_response(200, commits.to_json)
      rescue TypeError => e
        resp = generate_response(400, {"error" => e}.to_json)
        puts e
      end
      resp
    end



    #-------------------------
    # Contributors Controller
    #-------------------------
    def self.get_contributors(rid, initial_date: nil, final_date: nil)
      using_date = !(initial_date.nil? && final_date.nil?) # at least one isn't nil
      resp = validate_rid rid
      if resp["status"] != 200
        return resp
      end
      # validate date parameters
      if using_date
        resp_date = validate_date_params(initial_date, final_date)
        if resp_date["status"] == 200
          init_date, end_date = [initial_date, final_date].map{|d| Date.strptime(d, "%Y-%m-%d")}
        else
          return resp_date
        end
      end

      contributors = {}
      begin
        repo = RepoManager.rugged_repository rid
        walker = Rugged::Walker.new(repo)
        walker.push(repo.head.target)

        walker.each do |commit|
          commit_time = Time.at(commit.epoch_time).to_datetime
          if using_date && (commit_time < init_date || commit_time > end_date)
            next
          end

          hash = commit.oid

          [commit.author, commit.committer].each do |cont|
            name = cont[:name]
            email = cont[:email]
            if contributors.include? email
              contributor = contributors[email]
              if commit_time < contributor[:first_contribution]
                contributor[:first_contribution] = commit_time
              elsif commit_time > contributor[:last_contribution]
                contributor[:last_contribution] = commit_time
              end
              contributor[:commits] << hash
            else
              contributors[email] = {
                email: email,
                name: name,
                first_contribution: commit_time,
                last_contribution: commit_time,
                commits: [hash]
              }
            end
          end
        end
        resp = generate_response(200, contributors.values.to_json)
      rescue ArgumentError => e
        resp = generate_response(400, {"error" => e}.to_json)
        puts e
      end
      resp
    end

  end
end
