require_relative 'gret_tree'
require_relative 'cli_parser'
require_relative 'gret_config'
require_relative 'routing'

module Gret
  class CLI

    def self.run options
      if not (options.keys & [:key, :value]).empty?
        raise "Unsupported options for run command"
      end
      port = if options.key? :port then options[:port] else 4567 end
      path = if options.key? :path then options[:path] else Dir.pwd end
      _run path, port
    end

    def self._run path, port
      begin
        GretTree.init_from_path path
      rescue Exception => e
        raise "Failed to use #{path}: #{e.message}"
      end
      Routing.set :port, port
      Routing.set :bind, '0.0.0.0'
      puts "Gret v#{VERSION}"
      puts "Starting server at http://#{Routing.settings.bind}:#{Routing.settings.port}"
      puts "Using gret-tree: #{GretTree.instance.path}"
      puts ""
      Routing.run!
    end

    def self.init options
      if not (options.keys & [:port, :key, :value]).empty?
        raise "Unsupported options for init command"
      end
      path = if options.key? :path then options[:path] else Dir.pwd end
      _init path
    end

    def self._init path
      begin
        GretTree.create path
      rescue Exception => e
        raise "Failed to init gret-tree at #{path}: #{e.message}"
      end
    end

    def self.assert_gret_tree_in path
      if not GretTree.is_valid_gret_tree? path
        puts "error: #{path} is not a valid gret-tree."
        raise "Are you pointing to the right directory?"
      end
    end

    def self.config options
      if options.key? :port
        raise "User shouldn't specify a port for config command"
      end
      if not options.key? :key
        raise "User should specify a key for config command"
      end
      path = if options.key? :path then options[:path] else Dir.pwd end
      if options.key? :value
        set_config path, options[:key], options[:value]
      else
        show_config path, options[:key]
      end
    end

    def self.set_config path, key, value
      assert_gret_tree_in path
      GretConfig.load File.join(path, "gret.config")
      GretConfig.set key.to_sym, value
      puts "#{key} is now set to: #{value}"
    end

    def self.show_config path, key
      assert_gret_tree_in path
      GretConfig.load File.join(path, "gret.config")
      begin
        value = GretConfig.get(key.to_sym).to_s
      rescue
        puts "error: Configuration #{key} not present in 'gret.config' file."
      else
        puts "#{key}: #{value}"
      end
    end

    def self.main args
      parser = CLIParser.new
      options = parser.parse_cli
      unless options[:command] == ""
        begin
          CLI.public_send(options[:command], options)
        rescue Exception => e
          puts e.message
        end
      end
    end
  end
end
