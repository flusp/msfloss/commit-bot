require 'sequel'
# methods for setting up the database

module Gret
  class DatabaseConfig
    def self.setup_database db_path
      # Connect to the new database
      db = DatabaseConfig.connect_database(db_path)
      # Create the schema
      DatabaseConfig.create_schema(db)
      # Disconnect from the server
      db.disconnect
    end

    def self.create_schema db
      # Create the repo_trackers table
      db.create_table :repo_trackers do
        primary_key :rid
        String :url, :unique => true, :null => false
        String :status, size: 50
        DateTime :last_update
      end
    end

    def self.connect_database db_path
      Sequel.connect("sqlite://#{db_path}/gret.db")
    end

    def self.freeze_schema db
      # this method is mostly for performance reasons, it does
      # associations between tables Require all models, dynamically
      Dir[File.join(__dir__, "models", "*.rb")].each{|f| require f}
      # freeze model classes and database beforehand
      # (http://sequel.jeremyevans.net/rdoc/files/doc/code_order_rdoc.html)
      model_classes = Model.constants.map {|c|
        Model.const_get(c) if Model.const_get(c).is_a? Class
      }.compact
      model_classes.each(&:finalize_associations)
      model_classes.each(&:freeze)
      db.freeze
    end
  end
end
