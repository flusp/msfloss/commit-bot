require 'fileutils'
require_relative 'database_config'
require_relative 'gret_config'

module Gret
  # GretTree is a singleton
  # We don't use ruby's singleton module because we want to manually
  # control the instance creation.
  #
  # Note: One of GretTree.create or GretTree.init_from_path must be
  # called to initialize this class. It's an error to use the class
  # before doing it.
  class GretTree
    attr_reader :path, :repos_path, :gret_lock_path, :db_path
    private_class_method :new

    GRET_LOCK = "gret.lock"
    GRET_CONFIG = "gret.config"
    REPOS_DIR = "repositories"
    DB_DIR = "db"
    @@instance = nil

    def initialize path
      @path = path
      @gret_lock = nil
      @gret_lock_path = File.join(@path, GRET_LOCK)
      if not self.class.is_valid_gret_tree? path
              raise ArgumentError, "not a valid gret-tree"
      end
      self.acquire_lock
      @repos_path = File.join(@path, REPOS_DIR)
      @db_path = File.join(@path, DB_DIR)
      @db = DatabaseConfig.connect_database(@db_path)
      DatabaseConfig.freeze_schema(@db)
      GretConfig.load File.join(path, GRET_CONFIG)
    end

    def self.instance
      if not @@instance then raise "BUG: GretTree not initialized yet" end
      @@instance
    end

    def acquire_lock
      @gret_lock = File.open(@gret_lock_path)
      if not @gret_lock.flock(File::LOCK_EX | File::LOCK_NB)
        @gret_lock.close()
        raise IOError, "failed to acquire lock at #{@gret_lock_path}"
      end
    end

    def unlock
      if not @gret_lock then raise "Failed not locked" end
      @gret_lock.flock(File::LOCK_UN)
      @gret_lock.close()
    end

    def self.is_valid_gret_tree? path
      File.exist? File.join(path, GRET_LOCK) and
        File.exist? File.join(path, GRET_CONFIG) and
        Dir.exist? File.join(path, REPOS_DIR) and
        Dir.exist? File.join(path, DB_DIR)
    end

    def self.init_from_path path
      if @@instance then raise "BUG: already initialized gret-tree" end
      @@instance = new path
    end

    def self.create path
      if @@instance then raise "BUG: already initialized gret-tree" end
      if not File.directory? path then raise ArgumentError, "not a directory" end
      if not Dir.empty? path
        if self.is_valid_gret_tree? path
          raise ArgumentError, "is already a gret-tree"
        end
        raise ArgumentError, "directory is not empty"
      end
      # May raise exceptions that self.create's caller must rescue
      FileUtils.touch File.join(path, GRET_LOCK)
      GretConfig.create File.join(path, GRET_CONFIG)
      Dir.mkdir File.join(path, REPOS_DIR)
      Dir.mkdir File.join(path, DB_DIR)
      DatabaseConfig.setup_database(File.join(path, DB_DIR))
      @@instance = new path
    end
  end
end
