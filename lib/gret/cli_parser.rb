# frozen_string_literal: true

require 'optparse'

# rubocop:disable Metrics/MethodLength
module Gret
  # Responsible for parsing CLI commands for Gret
  class CLIParser
    attr_accessor :options, :opt_parser

    def initialize
      @options = {}
      @commands = %w[init run config]
      @opt_parser = init_parser_options
    end

    def init_parser_options
      OptionParser.new do |opts|
        opts.banner = "Usage: gret [command] [options]\n\n"
        opts.separator("Available commands:

        init                         Initializes gret-tree in specified path.
                                     Default path is current directory.

        run                          Runs gret in a specified path.
                                     Path must be a valid gret-tree.
                                     Default path is current directory.

        config                       Allows inspection or modification of
                                     options for a gret-tree in specified path.
                                     Default path is current directory.")
        opts.separator "Common options:\n\n"

        help_option(opts)
        version_option(opts)

        opts.separator "\nSpecific options:\n\n"

        port_option(opts)
        path_option(opts)
        key_option(opts)
        value_option(opts)
      end
    end

    def help_option(opts)
      opts.on('-h', '--help', 'Shows this message.') do
        puts @opt_parser
        exit
      end
    end

    def version_option(opts)
      opts.on('-v', '--version', 'Shows version.') do
        puts "v#{VERSION}"
        exit
      end
    end

    def port_option(opts)
      opts.on('--port PORT', 'Specifies a port to run the server at.',
              'Default port is 4567.', ' ') do |port|
        @options[:port] = port
      end
    end

    def path_option(opts)
      opts.on('--path PATH', 'Specifies a path pointing to a valid gret-tree.',
              'Default path is current directory.', ' ') do |path|
        @options[:path] = path
      end
    end

    def key_option(opts)
      opts.on('--key KEY', 'Specifies a configuration option.',
              'Required when using config command.', ' ') do |key|
        @options[:key] = key
      end
    end

    def value_option(opts)
      opts.on('--value VAL', 'Specifies value to set a '\
                                  'configuration option') do |val|
        @options[:value] = if numeric? val then to_numeric! val
                           else check_bool! val
                           end
      end
    end

    def numeric?(str)
      !Float(str).nil?
    rescue StandardError
      false
    end

    def to_numeric!(str)
      Integer(str)
    rescue StandardError
      begin
        Float(str)
      rescue StandardError
        return str
      end
    end

    def check_bool!(str)
      return true if str =~ /^(true)$/i

      return false if str.empty? || str =~ /^(false)$/i

      str
    end

    def parse_cli
      begin
        @opt_parser.parse!
      rescue StandardError
        abort 'error: Invalid option. Use -h or --help to get help.'
      end
      if !ARGV.empty? && @commands.include?(ARGV[0])
        @options[:command] = ARGV[0]
      else
        puts @opt_parser
        exit
      end
      options
    end
  end
end
# rubocop:enable Metrics/MethodLength
