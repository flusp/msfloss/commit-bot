# frozen_string_literal: true

require 'rugged'
require 'sequel'
require 'json'
require 'singleton'

module Gret
  # Manages the cloning of repositories, possibly in a
  # separate process (created through fork)
  class RepoCloner
    include Singleton
    def initialize
      @cloner_pid = nil
      @pipe_rd = nil
      @pipe_wr = nil
    end

    # Encapsulates the parameters that encode a repo
    # cloning task
    class Task
      attr_accessor :url, :rid, :path
      def initialize(url, rid, path)
        @path = path
        @url = url
        @rid = rid
      end

      def as_json
        { 'path' => @path, 'url' => @url, 'rid' => @rid }.to_json
      end

      def self.from_json(json_str)
        h = JSON.parse json_str
        new h['url'], h['rid'], h['path']
      end
    end

    def async_clone(url, rid, path)
      check_cloner_process
      task = RepoCloner::Task.new url, rid, path
      @pipe_wr.puts task.as_json
    rescue StandardError => e
      puts "Failed to launch async_clone: #{e}"
      raise 'Internal server error.'
    end

    def clone(url, rid, path)
      perform_clone(RepoCloner::Task.new(url, rid, path))
    end

    def check_cloner_process
      return if !@cloner_pid.nil? && proc_running?(@cloner_pid)

      @pipe_rd, @pipe_wr = IO.pipe if @pipe_wr.nil?
      @cloner_pid = fork do
        @pipe_wr.close
        @pipe_rd.each do |line|
          task = RepoCloner::Task.from_json line
          perform_clone task
        end
      end
    end

    # From: http://dev.housetrip.com/2014/03/24/ruby-pid-tip/
    def proc_running?(pid)
      Process.kill(0, pid)
      true
    rescue Errno::ESRCH
      false
    end

    def perform_clone(task)
      Rugged::Repository.clone_at(task.url, task.path, bare: true)
      Model::RepoTracker.update(task.rid,
                                status: 'tracking',
                                last_update: DateTime.now)
    rescue StandardError => e
      puts "Failed to clone '#{task.url}': #{e}"
      Model::RepoTracker.update(task.rid,
                                status: 'clone failed',
                                last_update: DateTime.now)
    end

    private :check_cloner_process, :proc_running?, :perform_clone
  end
end
