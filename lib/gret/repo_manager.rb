require 'rugged'
require 'sequel'

module Gret
  class RepoManager

    def self.clone url
      repo_tracker = Model::RepoTracker.get_from_url(url).first
      if not repo_tracker.nil?
        if ["tracking", "cloning"].include? repo_tracker[:status]
          raise TypeError, "#{url} is already being tracked or cloned"
        end
        Model::RepoTracker.update(
          repo_tracker[:rid], {status: "cloning", last_update: DateTime.now}
        )
      else
        repo_tracker = Model::RepoTracker.put(
          url: url, status: "cloning", last_update: DateTime.now
        )
        if repo_tracker.nil?
          raise "Internal database error"
        end
        repo_tracker = repo_tracker.values
      end

      rid = repo_tracker[:rid]
      # Can happen if cloning a repo which previously failed
      if Dir.exist?(self.path rid)
        FileUtils.rm_r(self.path rid)
      end
      if GretConfig.get :async_clone
        RepoCloner.instance.async_clone url, rid, self.path(rid)
      else
        RepoCloner.instance.clone url, rid, self.path(rid)
      end

      repo_tracker
    end

    def self.exists? rid
      not Model::RepoTracker.get(rid).first.nil?
    end

    def self.exists_from_url? url
      not Model::RepoTracker.get_from_url(url).first.nil?
    end

    def self.remove rid
      if exists? rid
        Model::RepoTracker.get(rid).delete
        FileUtils.rm_r(self.path rid) if Dir.exist?(self.path rid)
        return true
      end
      false
    end

    def self.path rid
      File.join(GretTree.instance.repos_path, rid.to_s)
    end

    def self.available_to_query? rid
      repo_tracker = Model::RepoTracker.get(rid).first
      not (repo_tracker.nil? or repo_tracker[:status] != "tracking")
    end

    def self.rugged_repository rid
      if not self.available_to_query? rid
        raise ArgumentError, "repo with rid #{rid} is not available to query"
      end
      Rugged::Repository.new(self.path rid)
    end
  end
end
