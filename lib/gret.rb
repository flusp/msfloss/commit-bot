# frozen_string_literal: true

require 'gret/version'
require 'gret/routing'
require 'gret/cli'
require 'gret/repo_manager'
require 'gret/api_controller'
require 'gret/gret_config'
require 'gret/repo_cloner'

module Gret
  class Error < StandardError; end

  # Do not add code here. Create files at lib/ and
  # require then at this file's header
end
