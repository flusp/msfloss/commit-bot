FROM ubuntu

MAINTAINER msfloss_group@gmail.com

RUN \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive \
    apt-get install -qq \
    sqlite3 \
    libsqlite3-dev \
    pkg-config \
    openssl \
    libssl-dev \
    ruby \
    ruby-dev \
    cmake
RUN mkdir /gret
COPY . /gret
WORKDIR /gret
RUN gem update --system
RUN gem install bundler
RUN gem update --system
RUN bundle install
RUN mkdir /repos
RUN bundle exec bin/gret init --path /repos
RUN bundle exec bin/gret config --path /repos --key show_exceptions --value false

EXPOSE 4567

ENTRYPOINT ["/gret/docker-compose-runner.sh"]
