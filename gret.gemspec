# frozen_string_literal: true

lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gret/version'

Gem::Specification.new do |spec|
  spec.name = 'gret'
  spec.version = Gret::VERSION

  spec.authors = [
    'Ângelo Lovatto', 'Juliano Garcia', 'Matheus Tavares',
    'Murilo Ribeiro', 'Vinicius Agostini'
  ]

  spec.email = [
    'angelolovatto@gmail.com',
    'julianogarcia_1997@hotmail.com',
    'matheus.bernardino@usp.br',
    'muriloborgesribeiro@gmail.com',
    'vinicius.agostini19@gmail.com'
  ]

  spec.summary = 'A tool for monitoring git repositories'
  spec.description = 'This bot collects statistics from git repositories'
  spec.homepage = 'https://gitlab.com/flusp/msfloss/commit-bot'

  # Specify which files should be added to the gem when it is released.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    Dir.glob('**/*', File::FNM_DOTMATCH).reject do |f|
      f.match(%r{^(test|spec|features)/})
    end
  end

  spec.bindir        = 'bin'
  spec.executables   = ['gret']
  spec.require_paths = ['lib']
  spec.required_ruby_version = '>= 2.4.0'

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop', '~>0.66.0'

  spec.add_dependency 'rack-test'
  spec.add_dependency 'rugged'
  spec.add_dependency 'sequel'
  spec.add_dependency 'simplecov'
  spec.add_dependency 'sinatra'
  spec.add_dependency 'sinatra-cross_origin'
  spec.add_dependency 'sqlite3'
end
